Frontend image gallery jQuery plugin

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/imagegallerybundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\ImageGalleryBundle\XLabsImageGalleryBundle(),
    ];
}
```

### Install assets ###
```bash
php app/console assets:install --symlink
```

### Usage ###
Append this anywhere in you template
```php
<link rel="stylesheet" type="text/css" href="{{ asset('bundles/xlabsimagegallery/xlabs.imagegallery.css') }}" />
<script type="text/javascript" src="{{ asset('bundles/xlabsimagegallery/xlabs.fullscreen.js') }}"></script>
<script type="text/javascript" src="{{ asset('bundles/xlabsimagegallery/xlabs.imagegallery.js') }}"></script>
```

Check example file to see how it works:
```php
bundles/xlabsimagegallery/index.html
```