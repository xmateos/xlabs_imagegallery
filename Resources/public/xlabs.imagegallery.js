// ImageGallery plugin v1.0 - jQuery image gallery plugin
// (c) 2017 Xavi Mateos - http://es.linkedin.com/pub/xavi-mateos/12/8ab/529
// License: http://www.opensource.org/licenses/mit-license.php
(function(window, $){
    var imageGallery_instances = 0; // set unique ID when multiple instances are required

    var imageGallery = function(elem, options){
        this.elem = elem;
        this.$elem = $(elem);
        this.$elem.data('plugin.imageGallery', this);
        this.options = options;
        this.metadata = this.$elem.data('xlabs-imageGallery-options');
        this.init();
    };

    imageGallery.prototype = {
        defaults: {
            child_selector: false,
            preview_container_only: false, // if true, thumbnail will be hidden and preview container will be displayed with navigation
            onLoadCallback: function(plugin){},
            clickCallback: function(e, elem){},
            previous: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            next: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
            close: '<i class="fa fa-times" aria-hidden="true"></i>',
            styles: {
                preview_container: false,
                preview_link: false,
                preview_image: false
            },
            fullscreen: false, // on preview click, expand gallery to full screen
            bgColor: '#fafafa',
            fullscrennBgColor: '#ECEFF4',
            bgColorButtons: '#fafafa',
            colorButtons: '#202020'
        },
        init: function(){
            this.config = $.extend({}, this.defaults, this.options, this.metadata);
            this.$elem.data('plugin.imageGallery', this);
            let plugin = this;

            // Fix for scrollTop
            plugin.$elem.data('scrollTop_pos', 0);

            plugin.$elem.addClass('xlabs_jqp_imageGallery');

            // Cloned item where to load the preview
            let layer_item = plugin.$elem.find(plugin.config.child_selector + ':first').clone();
            layer_item.empty().addClass('xlabs_jqp_imageGallery_layer_item');

            // Set custom class to all children
            plugin.$elem.find(plugin.config.child_selector).addClass('xlabs_jqp_imageGallery_item');
            layer_item.removeClass('xlabs_jqp_imageGallery_item');
            // Set indexes
            plugin.$elem.children('.xlabs_jqp_imageGallery_item').each(function(i, item){
                $(item).attr('data-xlabs-index', i + 1);
            });

            // Custom viewer container
            let preview_container = plugin.$elem.find('.xlabs_jqp_imageGallery_preview_container');
            preview_container = preview_container.length ? preview_container : $('<div />').addClass('xlabs_jqp_imageGallery_preview_container').css({
                'background-color': plugin.config.bgColor
            });
            plugin.config.styles.preview_container ? preview_container.addClass(plugin.config.styles.preview_container) : false;

            // Not found
            let not_found = $('<div />').addClass('xlabs_jqp_imageGallery_not_found').append('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>');

            // Arrows
            let previous = $('<a />').attr({
                'href': 'javascript:;'
            }).addClass('arrow').addClass('xlabs_prev').css({
                'background-color': plugin.config.bgColorButtons,
                'color': plugin.config.colorButtons
            }).append($(plugin.config.previous).css({
                'color': plugin.config.colorButtons
            }));
            let next = $('<a />').attr({
                'href': 'javascript:;'
            }).addClass('arrow').addClass('xlabs_next').css({
                'background-color': plugin.config.bgColorButtons,
                'color': plugin.config.colorButtons
            }).append($(plugin.config.next).css({
                'color': plugin.config.colorButtons
            }));
            let close = $('<a />').attr({
                'href': 'javascript:;'
            }).addClass('close').addClass('xlabs_close').css({
                'background-color': plugin.config.bgColorButtons,
                'color': plugin.config.colorButtons
            }).append($(plugin.config.close).css({
                'color': plugin.config.colorButtons
            }));
            preview_container
                .append(previous)
                .append(next)
                .append(close);
            if(plugin.config.preview_container_only)
            {
                close.hide();
            }

            next.on('click', function(){
                let next_index = plugin.getNextToLoad(1, plugin.getCurrentSelectedIndex());
                plugin.$elem.children('.xlabs_jqp_imageGallery_item').eq(next_index).trigger('click');
            });

            previous.on('click', function(){
                let previous_index = plugin.getNextToLoad(-1, plugin.getCurrentSelectedIndex());
                plugin.$elem.children('.xlabs_jqp_imageGallery_item').eq(previous_index).trigger('click');
            });

            close.on('click', function(){
                plugin.hideLayer();
            });

            // Keyboard
            $('body').keydown(function(e) {
                if(plugin.$elem.find('.xlabs_jqp_imageGallery_layer_item').is(':visible'))
                {
                    e.preventDefault();
                    switch(e.keyCode)
                    {
                        case 37: // left
                            if(previous.is(':visible'))
                            {
                                previous.trigger('click');
                            }
                            break;
                        case 32: // spacebar
                        case 39: // right
                            if(next.is(':visible'))
                            {
                                next.trigger('click');
                            }
                            break;
                    }
                }
            });

            // Image
            let preview_wrapper = $('<div />').addClass('xlabs_jqp_imageGallery_preview_wrapper');
            let preview_image = $('<img />').addClass('xlabs_jqp_imageGallery_preview_image');
            plugin.config.styles.preview_image ? preview_image.addClass(plugin.config.styles.preview_image) : false;
            let preview_link = $('<a />').addClass('xlabs_jqp_imageGallery_preview_link');
            plugin.config.styles.preview_link ? preview_image.addClass(plugin.config.styles.preview_link) : false;
            let preview_preloader = $('<figure class="xlabs_jqp_imageGallery_preview_preloader"><div class="dot white"></div><div class="dot"></div><div class="dot"></div><div class="dot"></div><div class="dot"></div></figure>');
            preview_container.append(
                preview_wrapper.append(
                    preview_link
                        .append(preview_image)
                        .append(preview_preloader)
                        .append(not_found)
                )
            );

            preview_container.appendTo(layer_item);
            plugin.$elem.append(layer_item.hide());
            plugin.$elem.data('layer_item', layer_item);

            if(plugin.config.fullscreen)
            {
                if(typeof window.fullscreen === "function")
                {
                    plugin.$elem.fullscreen({
                        onInit: function(){
                            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_link').on('click', function(e){
                                e.preventDefault();
                                plugin.$elem.fullscreen().enter(e, plugin.$elem.data('layer_item'));
                                return false;
                            });
                        },
                        onActive: function(){
                            if(plugin.config.preview_container_only)
                            {
                                close.show();
                            }
                            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_container').css({
                                'background-color': plugin.config.fullscrennBgColor
                            }).addClass('fullscreen');
                            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_link').addClass('fullscreen');
                            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_image').addClass('fullscreen');
                            // Show HR image
                            plugin.showImage(plugin.$elem.find('.xlabs_selected').attr('data-xlabs-gallery-link'));
                            /*document.addEventListener('keydown', function(e){
                                //left = 37
                                //up = 38
                                //right = 39
                                //down = 40
                                if(e.keyCode == 37) {
                                    plugin.$elem.find('.xlabs_prev').trigger('click');
                                }
                                if(e.keyCode == 39) {
                                    plugin.$elem.find('.xlabs_next').trigger('click');
                                }
                            }, false);*/
                        }, // element has changed to fullscreen
                        onInactive: function(){
                            if(plugin.config.preview_container_only)
                            {
                                close.hide();
                            }
                            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_container').css({
                                'background-color': plugin.config.bgColor
                            }).removeClass('fullscreen');
                            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_link').removeClass('fullscreen');
                            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_image').removeClass('fullscreen');
                            // Show medium image
                            plugin.showImage(plugin.$elem.find('.xlabs_selected').attr('data-xlabs-gallery-preview'));
                        }, // element has exit fullscreen
                    });
                } else {
                    plugin.config.fullscreen = false;
                    console.log('You need to import xlabs.fullscreen.js in order to use fullscreen support.');
                }
            }

            plugin.$elem.children('.xlabs_jqp_imageGallery_item').each(function(){
                $(this).on('click', function(e){
                    if(!this.hasAttribute('data-xlabs-gallery-skip'))
                    {
                        e.preventDefault();
                    }
                    plugin.loadImage(e, $(this));
                    return false;
                });
            });

            if(plugin.config.preview_container_only)
            {
                plugin.$elem.children('.xlabs_jqp_imageGallery_item').hide();
                plugin.$elem.find('.xlabs_jqp_imageGallery_item:first').trigger('click');
            }

            plugin.config.onLoadCallback(this);
        },
        loadImage: function(e, elem){
            let plugin = this;
            plugin.highlightNextToLoad(e, elem);

            if(!elem[0].hasAttribute('data-xlabs-gallery-skip'))
            {
                // Set preview image
                if(plugin.config.fullscreen && plugin.$elem.fullscreen().isActive())
                {
                    plugin.showImage(elem.attr('data-xlabs-gallery-link'));
                } else {
                    plugin.showImage(elem.attr('data-xlabs-gallery-preview'));
                }

                if(elem.attr('data-xlabs-gallery-link'))
                {
                    plugin.setImageLink(elem);
                }
            }

            plugin.showPagination(parseInt(elem.attr('data-xlabs-index')));

            plugin.showPreview(e, elem);

            if(elem[0].hasAttribute('data-xlabs-gallery-callback'))
            {
                let custom_callback = elem.attr('data-xlabs-gallery-callback');
                try {
                    custom_callback = eval(custom_callback);
                    if(typeof custom_callback == 'function')
                    {
                        custom_callback(e, elem);
                    }
                }
                catch(err) {
                    console.log('XLabs Gallery (data-xlabs-gallery-callback): ' + err.message);
                }
            }

            plugin.itemClickedCallback(e, elem);
        },
        showLayer: function(){
            let plugin = this;
            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_container').show();
            plugin.$elem.data('layer_item').show();
        },
        hideLayer: function(){
            let plugin = this;
            if(plugin.config.fullscreen && plugin.$elem.fullscreen().isActive())
            {
                plugin.$elem.fullscreen().exit();
            } else {
                plugin.$elem.data('layer_item').hide();
                plugin.removeAllHighlights();
                plugin.hideImage();
            }
        },
        showImage: function(url){
            let plugin = this;
            let imgHandler = new Image();
            imgHandler.onload = function(){
                plugin.$elem.find('.xlabs_jqp_imageGallery_not_found').hide();
                plugin.$elem.find('.xlabs_jqp_imageGallery_preview_image').attr('src', url);
                plugin.$elem.find('.xlabs_jqp_imageGallery_preview_image').show();
                plugin.hidePreloader();
            };
            imgHandler.onerror = function(){
                plugin.hideImage();
                plugin.hidePreloader();
                plugin.disableImageLink();
                plugin.$elem.find('.xlabs_jqp_imageGallery_not_found').show();
            };
            plugin.showPreloader();
            imgHandler.src = url;
        },
        hideImage: function(){
            let plugin = this;
            plugin.hidePreloader();
            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_image').hide();
        },
        showPreloader: function(){
            let plugin = this;
            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_image').addClass('preloading');
            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_preloader').show();
            plugin.$elem.find('.xlabs_jqp_imageGallery_not_found').hide();
        },
        hidePreloader: function(){
            let plugin = this;
            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_image').removeClass('preloading');
            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_preloader').hide();
        },
        setImageLink: function(elem){
            let plugin = this;
            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_link').attr('href', elem.attr('data-xlabs-gallery-link'));
            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_link').attr('target', elem.attr('data-xlabs-gallery-link-target') ? elem.attr('data-xlabs-gallery-link-target') : '_self');
        },
        disableImageLink: function(elem){
            let plugin = this;
            plugin.$elem.find('.xlabs_jqp_imageGallery_preview_link').attr('href', 'javascript:;');
        },
        highlightNextToLoad: function(e, elem){
            let plugin = this;
            plugin.$elem.find('.xlabs_selected').removeClass('xlabs_selected');
            elem.addClass('xlabs_selected');
        },
        removeAllHighlights: function(){
            let plugin = this;
            plugin.$elem.find('.xlabs_selected').removeClass('xlabs_selected');
        },
        getCurrentSelectedIndex: function(){
            let plugin = this;
            return plugin.$elem.children('.xlabs_jqp_imageGallery_item.xlabs_selected').attr('data-xlabs-index');
        },
        getTotalChildren: function(){
            let plugin = this;
            return parseInt(plugin.$elem.children('.xlabs_jqp_imageGallery_item').length);
        },
        getNextToLoad: function(incr, currentSelectedIndex){
            let next_to_load = currentSelectedIndex - 1 + parseInt(incr);
            return next_to_load;
        },
        showPagination: function(elemIndexToBeLoaded){
            let plugin = this;
            switch(elemIndexToBeLoaded)
            {
                case 1:
                    plugin.$elem.find('.xlabs_prev').hide();
                    (plugin.getTotalChildren() == 1) ? plugin.$elem.find('.xlabs_next').hide() : plugin.$elem.find('.xlabs_next').show();
                    break;
                case plugin.getTotalChildren():
                    plugin.$elem.find('.xlabs_prev').show();
                    plugin.$elem.find('.xlabs_next').hide();
                    break;
                default:
                    plugin.$elem.find('.xlabs_prev').show();
                    plugin.$elem.find('.xlabs_next').show();
                    break;
            }
        },
        getFirstItemOfCurrentRow: function(e, elem) {
            let plugin = this;
            let selected_elem_index = elem.attr('data-xlabs-index');
            let x = plugin.$elem.children('.xlabs_jqp_imageGallery_item').filter(function(i, item){
                return (parseInt($(item).attr('data-xlabs-index')) < parseInt(selected_elem_index)) && (parseInt(Math.round($(item).position().left)) <= parseInt(Math.round(elem.position().left)));
            });
            return x.first();
        },
        getFirstItemOfNextRow: function(e, elem) {
            let plugin = this;
            let selected_elem_index = elem.attr('data-xlabs-index');
            let x = plugin.$elem.children('.xlabs_jqp_imageGallery_item').filter(function(i, item){
                return (parseInt($(item).attr('data-xlabs-index')) > parseInt(selected_elem_index)) && (parseInt(Math.round($(item).position().left)) <= parseInt(Math.round(elem.position().left)));
            });
            return x.first();
        },
        getLastItemOfPreviousRow: function(e, elem) {
            let plugin = this;
            let selected_elem_index = elem.attr('data-xlabs-index');
            let x = plugin.$elem.children('.xlabs_jqp_imageGallery_item').filter(function(i, item){
                return (parseInt($(item).attr('data-xlabs-index')) < parseInt(selected_elem_index)) && (parseInt(Math.round($(item).position().left)) >= parseInt(Math.round(elem.position().left)));
            });
            return x.last();
        },
        showPreview: function(e, elem){
            let plugin = this;

            // Show preview nicely on last and unique rows
            let firstOfNextRow = plugin.getFirstItemOfNextRow(e, elem);
            let item = false;
            if(firstOfNextRow.length)
            {
                item = firstOfNextRow;
                if(parseInt(item.attr('data-xlabs-index')) != parseInt(plugin.$elem.data('layer_item').attr('data-xlabs-current-position')))
                {
                    if(!plugin.config.fullscreen || !plugin.$elem.fullscreen().isActive())
                    {
                        plugin.$elem.data('layer_item').attr('data-xlabs-current-position', item.attr('data-xlabs-index')).insertBefore(item);
                    }
                }
            } else {
                let lastOfPreviousRow = plugin.getLastItemOfPreviousRow(e, elem);
                if(lastOfPreviousRow.length)
                {
                    item = lastOfPreviousRow;
                    if(parseInt(item.attr('data-xlabs-index')) != parseInt(plugin.$elem.data('layer_item').attr('data-xlabs-current-position')))
                    {
                        if(!plugin.config.fullscreen || !plugin.$elem.fullscreen().isActive())
                        {
                            plugin.$elem.data('layer_item').attr('data-xlabs-current-position', item.attr('data-xlabs-index')).insertAfter(item);
                        }
                    }
                }
            }
            if(!item)
            {
                // there´s only 1 row of images
                item = plugin.getFirstItemOfCurrentRow(e, elem);
                item = item.length ? item : elem;
                if(parseInt(item.attr('data-xlabs-index')) != parseInt(plugin.$elem.data('layer_item').attr('data-xlabs-current-position')))
                {
                    if(!plugin.config.fullscreen || !plugin.$elem.fullscreen().isActive())
                    {
                        plugin.$elem.data('layer_item').attr('data-xlabs-current-position', item.attr('data-xlabs-index')).insertBefore(item);
                    }
                }

            }

            plugin.showLayer();

            // User friendly scroll
            if(!plugin.config.preview_container_only)
            {
                let top_pos = plugin.$elem.data('layer_item').offset().top;
                top_pos = parseInt(top_pos);
                if(top_pos != plugin.$elem.data('scrollTop_pos'))
                {
                    plugin.$elem.data('scrollTop_pos', top_pos);
                    top_pos -= item.height();
                    top_pos = parseInt(top_pos);
                    $('html, body').animate({scrollTop: top_pos}, 400);
                }
            }
        },
        itemClickedCallback: function(e, elem) {
            let plugin = this;
            if(typeof plugin.config.clickCallback == 'function')
            {
                plugin.config.clickCallback(e, elem);
            }
        },
        destroy: function() {
            ////this.$elem.data('plugin.imageGallery', false);
            //this.$elem.removeData('plugin.imageGallery');
            //this.$elem.find('button._load_more').parent().remove();
            //return this.$elem;
        }
    };

    imageGallery.defaults = imageGallery.prototype.defaults;

    $.fn.imageGallery = function(options = {}) {
        options.index = imageGallery_instances;
        imageGallery_instances++;
        return $(this).data('plugin.imageGallery') ? $(this).data('plugin.imageGallery') : new imageGallery(this, options);
        //return this.each(function() {});
    };

    window.imageGallery = imageGallery;
})(window, jQuery);