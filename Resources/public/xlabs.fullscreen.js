(function(window, $){
    var fullscreen_instances = 0; // set unique ID when multiple instances are required

    var fullscreen = function(elem, options){
        this.elem = elem;
        this.$elem = $(elem);
        this.$elem.data('plugin.fullscreen', this);
        this.options = options;
        this.metadata = this.$elem.data('xlabs-fullscreen-options');
        this.init();
    };

    fullscreen.prototype = {
        defaults: {
            onInit: function(){},
            onActive: function(){}, // element has changed to fullscreen
            onInactive: function(){}, // element has exit fullscreen
        },
        init: function(){
            this.config = $.extend({}, this.defaults, this.options, this.metadata);
            this.$elem.data('plugin.fullscreen', this);
            let plugin = this;

            let fullscreenAvailable = (
                document.fullscreenEnabled ||
                document.webkitFullscreenEnabled ||
                document.mozFullScreenEnabled ||
                document.msFullscreenEnabled
            );
            if(fullscreenAvailable)
            {
                document.addEventListener('fullscreenchange', function(){
                    plugin.statusChange(plugin);
                }, false);
                document.addEventListener('webkitfullscreenchange', function(){
                    plugin.statusChange(plugin);
                }, false);
                document.addEventListener('mozfullscreenchange', function(){
                    plugin.statusChange(plugin);
                }, false);
                document.addEventListener('MSFullscreenChange', function(){
                    plugin.statusChange(plugin);
                }, false);
                /*document.addEventListener("fullscreenchange", plugin.statusChange);
                document.addEventListener("webkitfullscreenchange", plugin.statusChange);
                document.addEventListener("mozfullscreenchange", plugin.statusChange);
                document.addEventListener("MSFullscreenChange", plugin.statusChange);*/

                document.addEventListener('fullscreenerror', function(){
                    plugin.error(plugin);
                }, false);
                document.addEventListener('webkitfullscreenerror', function(){
                    plugin.error(plugin);
                }, false);
                document.addEventListener('mozfullscreenerror', function(){
                    plugin.error(plugin);
                }, false);
                document.addEventListener('MSFullscreenError', function(){
                    plugin.error(plugin);
                }, false);
                /*document.addEventListener("fullscreenerror", plugin.error());
                document.addEventListener("webkitfullscreenerror", plugin.error());
                document.addEventListener("mozfullscreenerror", plugin.error());
                document.addEventListener("MSFullscreenError", plugin.error());*/

                plugin.config.onInit();
            }
        },
        enter: function(e, elem){
            if(elem.get(0).requestFullscreen) {
                elem.get(0).requestFullscreen();
            } else if (elem.get(0).webkitRequestFullscreen) {
                elem.get(0).webkitRequestFullscreen();
            } else if (elem.get(0).mozRequestFullScreen) {
                elem.get(0).mozRequestFullScreen();
            } else if (elem.get(0).msRequestFullscreen) {
                elem.get(0).msRequestFullscreen();
            }
        },
        exit: function(){
            let plugin = this;
            if(plugin.isActive())
            {
                if(document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                }
            }
        },
        statusChange: function(plugin){
            if(plugin.isActive())
            {
                plugin.config.onActive();
            } else {
                plugin.config.onInactive();
            }
        },
        error: function(plugin) {},
        isActive: function(){
            return document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
        },
        destroy: function() {
            ////this.$elem.data('plugin.fullscreen', false);
            this.$elem.removeData('plugin.fullscreen');
            //this.$elem.find('button._load_more').parent().remove();
            //return this.$elem;
        }
    };

    fullscreen.defaults = fullscreen.prototype.defaults;

    $.fn.fullscreen = function(options = {}) {
        options.index = fullscreen_instances;
        fullscreen_instances++;
        return $(this).data('plugin.fullscreen') ? $(this).data('plugin.fullscreen') : new fullscreen(this, options);
        //return this.each(function() {});
    };

    window.fullscreen = fullscreen;
})(window, jQuery);